﻿using System;
using System.Collections;

namespace comp6211_arraylists
{
    class Program
    {
        static void Main(string[] args)
        {
            courseClass.newStudent("John", 70);
            courseClass.newStudent("Jim", 86.54);
            courseClass.newStudent("Jane", 98.87);
            courseClass.newStudent("David", 43.6);
            courseClass.newStudent("Bob", 64.65);
            courseClass.newStudent("Sarah", 85.12);

            Console.WriteLine(courseClass.averageScore());
            Console.WriteLine(courseClass.highestScore());
            Console.WriteLine(courseClass.lowestScore());
            Console.WriteLine(courseClass.sortStudents());
            Console.WriteLine(courseClass.sortScores());
        }
    }

    class courseClass
    {
        private static ArrayList students = new ArrayList();
        private static ArrayList scores = new ArrayList();
        
        public static void newStudent(string newName, double newScore)
        {
            courseClass.students.Add(newName);
            courseClass.scores.Add(newScore);
        }

        static public double averageScore()
        {
            double mean = 0;

            for(int i = 0; i < students.Count; i++)
            {
                mean = mean + Convert.ToDouble(scores[i]);
            }

            mean = mean / students.Count;
            Math.Round(mean, 2);
            
            return mean;
        }

        static public double highestScore()
        {
            double highscore = 0;

            for(int i = 0; i < scores.Count; i++)
            {
                if(Convert.ToDouble(scores[i]) > highscore)
                {
                    highscore = Convert.ToDouble(scores[i]);
                }
            }

            return highscore;
        }

        static public double lowestScore()
        {
            double lowscore = 100;

            for(int i = 0; i < scores.Count; i++)
            {
                if(Convert.ToDouble(scores[i]) < lowscore)
                {
                    lowscore = Convert.ToDouble(scores[i]);
                }
            }

            return lowscore;
        }

        static public string sortStudents()
        {
            students.Sort();
            Console.WriteLine("Sorting by Student");
            for(int i = 0; i < students.Count; i++)
            {
                Console.WriteLine(students[i]);
            }

            return "Finished Sorting by Student";
        }

        static public string sortScores()
        {
            scores.Sort();
            Console.WriteLine("Sorting by Score");
            for(int i = 0; i < scores.Count; i++)
            {
                Console.WriteLine(scores[i]);
            }

            return "Finished Sorting by Score";
        }
    }
}
